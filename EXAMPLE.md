## Request examples

### Creating a wishlist:

```
curl -X POST http://localhost:8080/wishlists/ \
    -d '{"name": "anotherlist", "products": [{"id": 1}]}' \
    -H "Content-Type: application/json"
```

### Getting a wishlist:

```
curl http://localhost:8080/wishlists/2
```