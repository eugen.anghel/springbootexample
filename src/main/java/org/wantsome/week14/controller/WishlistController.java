package org.wantsome.week14.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.wantsome.week14.model.Wishlist;
import org.wantsome.week14.repo.WishlistRepository;

@Controller
@RequestMapping("/wishlists")
public class WishlistController {

    WishlistRepository wishlistRepository;

    public WishlistController(WishlistRepository repo) {
        this.wishlistRepository = repo;
    }

    @ResponseBody
    @PostMapping
    Wishlist create(@RequestBody  Wishlist wishlist) {
        return wishlistRepository.save(wishlist);
    }

    @ResponseBody
    @GetMapping("/{id}")
    Wishlist home(@PathVariable("id") long id) {
        return wishlistRepository.findOne(id);
    }
}
