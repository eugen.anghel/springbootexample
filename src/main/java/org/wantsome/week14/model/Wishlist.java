package org.wantsome.week14.model;


import javax.persistence.*;
import java.util.List;

@Entity
public class Wishlist {
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Id
    private Long id;
    private String name;

    @ManyToMany
    private List<Product> products;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wishlist wishlist = (Wishlist) o;

        if (id != null ? !id.equals(wishlist.id) : wishlist.id != null) return false;
        if (name != null ? !name.equals(wishlist.name) : wishlist.name != null) return false;
        return products != null ? products.equals(wishlist.products) : wishlist.products == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (products != null ? products.hashCode() : 0);
        return result;
    }
}

