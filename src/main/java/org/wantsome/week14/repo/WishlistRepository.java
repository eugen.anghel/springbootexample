package org.wantsome.week14.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.wantsome.week14.model.Wishlist;

@Repository
public interface WishlistRepository extends CrudRepository<Wishlist, Long> {

}
