insert into product(name, price) values('Vanilla Ice Cream', 10);
insert into product(name, price) values('Brown Leather Shoes', 430);
insert into product(name, price) values('Blue Sports Jacket', 500);

insert into wishlist(name) values('Wearables');
insert into wishlist_products(products_id, wishlist_id) values(2, 1);
insert into wishlist_products(products_id, wishlist_id) values(3, 1);
